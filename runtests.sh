#!/bin/bash

exitcode=0

for file in testsrcs/*
do
    echo "---------------------------------------------------------------------"
    echo "TESTING ($file)"
    ./bfci -t "$file"
    retval=$?
    echo
    if [ $retval -ne 2 ]; then
        echo "FAILED AT ($file): $retval"
        exitcode=1
    else
        echo "PASSED AT ($file): $retval"
    fi
done

exit $exitcode
